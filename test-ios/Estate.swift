//
//  Estado.swift
//  test-ios
//
//  Created by carlos chaguendo on 4/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

enum EstateType {
	case origen
	case destino
	case visited
	case frontera
	case disabled
	case none

}

class Estate: UIViewFromXIB {

	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!

	// Las cordenadas en las x,y en la matriz
	var coordenadas: CGPoint = CGPoint.zero

	// Las coordenadas convertidas a puntos en la matriz
	var punto: Int = 0

	// El costo que hay entre el estado y el destino
	let costo: CGFloat = CGFloat.infinity


	var size: Int = 0

	// si el estado ya esta visitado
	var type: EstateType = .none {
		didSet {
			switch type {
			case .origen:
				view.backgroundColor = UIColor.Hex(0x3182D9)
			case .destino:
				view.backgroundColor = UIColor.Hex(0x27916F)
			case .visited:
				view.backgroundColor = UIColor.orange
			case .frontera:
				view.backgroundColor = UIColor.orange.withAlphaComponent(0.2)
			case .disabled:
				view.backgroundColor = UIColor.Hex(0x757a91)
			case .none:
				view.backgroundColor = UIColor.white
			}
		}
	}

	internal override init(frame: CGRect) {
		super.init(frame: frame)
		layer.borderColor = UIColor.gray.cgColor
		layer.borderWidth = 0.5
		isUserInteractionEnabled = true


	}


	internal required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	convenience init(coordenadas: CGPoint, size: Int) {
		self.init(frame: CGRect.zero)
		self.coordenadas = coordenadas
		self.size = size


		let x = Int(coordenadas.x)
		let y = Int(coordenadas.y)
		self.punto = x + (y * size)

		textLabel.text = "\(punto)"
		detailLabel.text = "(\( Int( coordenadas.x)),\(Int(coordenadas.y)))"
	}


	func vecinos(matriz: [[Estate]]) -> [Estate] {

		var vecinos: [Estate] = []
		let x = Int(coordenadas.x)
		let y = Int(coordenadas.y)
		let check = x + y;

		var inicioX = x - 1
		var finX = x + 1

		var inicioY = y - 1
		var finY = y + 1;

		if(inicioY < 0) { inicioY = 0 }
		if(inicioX < 0) { inicioX = 0 }
		if(finX >= size) { finX = size-1 }
		if(finY >= size) { finY = size-1 }

		for i in inicioX...finX {
			for j in inicioY...finY {

				let suma = i + j;

				if(suma == check + 1 || suma == check - 1) {
					let state = matriz[i][j]
					if state.type == .none || state.type == .destino {
						vecinos.append(state)
					}
				}

			}
		}

		return vecinos

	}



}
