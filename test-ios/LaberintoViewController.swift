//
//  LaberintoViewController.swift
//  test-ios
//
//  Created by carlos chaguendo on 4/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit



class LaberintoViewController: ViewController {

	let CGPointDefault = CGPoint(x: -1, y: -1)

	var matriz: [[Estate]] = []
	var origen: CGPoint = CGPoint.zero
	var destino: CGPoint = CGPoint.zero

	var frontera: [Estate] = []
	var state: Estate?


	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.prompt = "Laberinto"
		createMatriz(size: 7, origen: origen, destino: destino)
		origen = CGPointDefault
		destino = CGPointDefault
	}


	/**
     
     */
	func createMatriz(size: Int, origen: CGPoint, destino: CGPoint) {

		let bordersOffset: CGFloat = 10
		let windowWidth = UIScreen.main.bounds.width - (2 * bordersOffset)
		let estateWidth = windowWidth / CGFloat(size)
		let estateSize = CGSize(width: estateWidth, height: estateWidth)

		matriz.removeAll()
		for i in 0..<size {
			var row: [Estate] = []


			for j in 0..<size {

				let estate = Estate(coordenadas: CGPoint(x: i, y: j), size: size)

				// se calcual el tamanio de cada estado
				let x = estateWidth * (CGFloat(i)) + bordersOffset
				let y = estateWidth * (CGFloat(j) + 2)
				let origin = CGPoint(x: x, y: y)

				let frame = CGRect(origin: origin, size: estateSize)
				estate.frame = frame

				self.view.addSubview(estate)

				row.append(estate)
			}
			matriz.append(row)
		}
	}


	func calculateAnchor(_ anchorPoint: CGPoint) -> Estate? {
		for i in 0...matriz.count - 1 {
			for j in 0...matriz[i].count - 1 {
				if matriz[i][j].frame.contains(anchorPoint) {
					return matriz[i][j]
				}
			}
		}
		return nil
	}


	@IBAction func clearMatriz(_ sender: Any) {
		origen = CGPointDefault
		destino = CGPointDefault
		matriz.forEach { $0.forEach { $0.type = .none } }
		frontera.removeAll()
	}

	@IBAction func nextAction(_ sender: Any) {
		guard let state = frontera.popLast() else { return }
		state.type = .visited

		print("Estado \(state.punto)")


		if state.coordenadas == destino {
			alertInformation("Leggaste", message: "Recorido", handler: {
				//self.clearMatriz(state)
			})
			return
		}


		let vecinos = state.vecinos(matriz: self.matriz)
		for vecino in vecinos {
			if vecino.type != .destino {
				vecino.type = .frontera
			}
			frontera.append(vecino)
		}


	}


	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {


		if let touch = touches.first {
			let touchPoint = touch.location(in: self.view)
			if let estate = calculateAnchor(touchPoint) {

				if origen == CGPointDefault {
					estate.type = .origen
					origen = estate.coordenadas
					frontera.append(estate)
					return
				}

				if destino == CGPointDefault {
					estate.type = .destino
					destino = estate.coordenadas
					return
				}

				if estate.type == .none {
					estate.type = .disabled
				} else if estate.type == .disabled {
					estate.type = .none
				}


			}
		}

	}




}
