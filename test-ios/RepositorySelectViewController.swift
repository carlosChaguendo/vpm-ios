//
//  RepositorySelectViewController.swift
//  test-ios
//
//  Created by carlos chaguendo on 20/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import PromiseKit

public class RepositorySelectViewController: LiveScrollTableViewController, UIPopoverPresentationControllerDelegate {


	private var repositoryIndex: IndexPath?


	override public func viewDidLoad() {
		super.viewDidLoad()
		popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "defaultCell")

	}

	/**
     Forza el modo popopver en el iphonee
     */
	public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
		return UIModalPresentationStyle.none
	}


	public override func liveScroll(valuesOf page: Int) {

		RepositoryService.repositories(for: "carlosChaguendo", page: page)
			.then(execute: { (result: SearchResult<Repository>?) -> Void in
				guard let repositories = result?.values else {
					self.hasMore = false
					return
				}

				if repositories.count <= 0 {
					self.hasMore = false
				}

				repositories.forEach({ self.values.append($0) })


				self.loadInformation = true;
				self.tableView.reloadData()
			}).always (execute: {
				self.loadInformation = true;
			}).catch (execute: self.presentError)

	}



	@IBAction func dissmisViewController(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}





	override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "defaultCell")
		guard let repository = values[safe: indexPath.row] as? Repository else {
			return cell
		}
		cell.textLabel?.text = repository.name
		cell.selectionStyle = .none
		return cell
	}

	public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let cell = tableView.cellForRow(at: indexPath) else {
			return
		}

		if let prevIndex = repositoryIndex {
			tableView.cellForRow(at: prevIndex)?.accessoryType = .none
		}
		repositoryIndex = indexPath
		cell.accessoryType = .checkmark
	}






}
