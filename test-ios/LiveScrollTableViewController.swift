//
//  LiveScrollTableViewController.swift
//  test-ios
//
//  Created by carlos chaguendo on 21/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import PromiseKit
import NVActivityIndicatorView

public class LiveScrollTableViewController: UITableViewController {

	public var values: [AnyObject] = []
	// pagination
	public var currentPage = 1
	public var hasMore = true
	public var loadInformation = false
	public var loadFromServer = false
	public var refreshActivitiIndicator: NVActivityIndicatorView?

	// Promise<SearchResult<Repository>?>
	public func liveScroll(valuesOf page: Int) {
		preconditionFailure()
	}

	override public func viewDidLoad() {
		super.viewDidLoad()


		if let refresh = refreshControl {
			let frame = refresh.frame
			let activity = NVActivityIndicatorView(frame: frame, type: .ballScaleMultiple, color: Colors.activity_indicator)
			refresh.addSubview(activity)
			refresh.tintColor = UIColor.clear
			activity.startAnimating()
			refresh.addTarget(self, action: #selector(self.loadValuesFromServer), for: UIControlEvents.valueChanged)

			refreshActivitiIndicator = activity
		}


		loadValues()
	}


	@objc private func loadValuesFromServer() {
		currentPage = 1
		loadFromServer = true
		liveScroll(valuesOf: currentPage)
		loadFromServer = false
	}


	private func loadValues() {
		liveScroll(valuesOf: currentPage)
	}

	override public func numberOfSections(in tableView: UITableView) -> Int {
		if !loadInformation {

			let view = UIView()
			tableView.backgroundView = view


			let frame = CGRect(x: view.center.x - 20, y: view.center.y - 20, width: 40, height: 40);
			let activity = NVActivityIndicatorView(frame: frame, type: .ballScale, color: Colors.activity_indicator)
			activity.startAnimating()
			view.addSubview(activity)

			tableView.separatorStyle = .none
			return 0
		}

		view.backgroundColor = UIColor.white
		tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
		tableView.backgroundView = nil
		return 1
	}

	override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return values.count
	}

	public override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

		let lastRow = self.values.count - 1
		if indexPath.row == lastRow {
			currentPage += 1
			if hasMore {
				loadValues()
			}
		}

	}






}
