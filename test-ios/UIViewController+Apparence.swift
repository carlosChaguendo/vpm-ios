//
//  UIViewController+Apparence.swift
//  test-ios
//
//  Created by carlos chaguendo on 19/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import ToastSwiftFramework


/**
 *
 */
extension UIImagePickerController {
	override open func viewDidLoad() {
		super.viewDidLoad()
	}
}



/**
 *
 */
extension UIViewController {

	func presentError(_ error: Error) -> Void {

		func showLocalizedDescription(_ nserror: Error) {
			if let description = (nserror as NSError?)?.localizedDescription {
				var style = ToastStyle()
				// style.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
				style.verticalPadding = 5.0
				self.view.makeToast("\(description)", duration: 5.0, position: .bottom, style: style)
			}
		}

		showLocalizedDescription(error)


	}

	func alertInformation(_ title: String, message: String, handler: @escaping (() -> Void)) {

		let buttonsHandler: (UIAlertAction) -> Void = { action in
			switch action.style {
			case .default: handler()
			default: break
			}
		}

		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: buttonsHandler))
		self.present(alert, animated: true, completion: nil)

	}
}

extension UITableViewController {

	override open func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = Colors.controller_background
	}


	func updateTableViewHeaderViewHeight() {
		guard let headerView = self.tableView.tableHeaderView else { return }
		headerView.translatesAutoresizingMaskIntoConstraints = false

		let headerWidth = headerView.bounds.size.width;
		let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[headerView(width)]", options: NSLayoutFormatOptions(rawValue: UInt(0)), metrics: ["width": headerWidth], views: ["headerView": headerView])

		headerView.addConstraints(temporaryWidthConstraints)

		headerView.setNeedsLayout()
		headerView.layoutIfNeeded()

		let headerSize = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
		let height = headerSize.height
		var frame = headerView.frame

		frame.size.height = height
		headerView.frame = frame

		self.tableView.tableHeaderView = headerView

		headerView.removeConstraints(temporaryWidthConstraints)
		headerView.translatesAutoresizingMaskIntoConstraints = true
	}
}

extension UIApplication {
	var statusBarView: UIView? {
		return value(forKey: "statusBar") as? UIView
	}
}

extension UINavigationController {
	open override func viewDidLoad() {
		super.viewDidLoad()
		hidesBarsOnSwipe = true
	}
}


class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = UIColor.Hex(0xeeeeee)
	}

}






