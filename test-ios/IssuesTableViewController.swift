//
//  IssuesTableViewController.swift
//  test-ios
//
//  Created by carlos chaguendo on 20/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import ToastSwiftFramework
import NVActivityIndicatorView
import DropDown



public class IssuesTableViewController: LiveScrollTableViewController {

	override public func viewDidLoad() {
		super.viewDidLoad()

		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "defaultCell")
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 250


	}


	override public func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.hidesBarsOnSwipe = false
	}

	override public func liveScroll(valuesOf page: Int) {


		IssuesService.issues(of: "mayorgafirm", inRepository: "adivantus-iphone", page: page, refreshFromServer: loadFromServer)
			.then (execute: { (result) -> Void in

				if self.loadFromServer {
					self.values.removeAll()
					self.tableView.reloadData()
				}

				guard let issues = result?.values else {
					self.hasMore = false
					return
				}

				if issues.count <= 0 {
					self.hasMore = false
				}

				issues.forEach({ self.values.append($0) })
				self.loadInformation = true;
				self.tableView.reloadData()
			}).always (execute: {
				self.loadInformation = true;
				self.refreshControl?.endRefreshing()
			}).catch (execute: self.presentError)
	}

	@IBAction func showRepositorySelector(_ sender: UIBarButtonItem) {


		// get a reference to the view controller for the popover
		let selectviewController = Storyboard.Issues.viewControllerWithClass(RepositorySelectViewController.self)
		let popController = UINavigationController(rootViewController: selectviewController)

		// set the presentation style
		popController.modalPresentationStyle = UIModalPresentationStyle.popover

		// set up the popover presentation controller
		popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
		popController.popoverPresentationController?.delegate = selectviewController
		popController.popoverPresentationController?.sourceView = sender.plainView
		popController.popoverPresentationController?.sourceRect = CGRect(x: sender.plainView.bounds.midX, y: sender.plainView.bounds.midY, width: 0, height: 0)

		popController.popoverPresentationController?.popoverBackgroundViewClass = Pop.self


		// present the popover
		self.present(popController, animated: true, completion: nil)
	}



	override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let cell = tableView.dequeueReusableCell(withIdentifier: "issueViewCell", for: indexPath) as? IssueTableViewCell else {
			return UITableViewCell(style: .subtitle, reuseIdentifier: "defaultCell")
		}

		guard let issue = values[safe: indexPath.row] as? Issue else {
			return cell
		}

		cell.issue = issue
		return cell
	}



	public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		guard let cell = sender as? IssueTableViewCell else {
			preconditionFailure()
		}

		if let controller = segue.destination as? IssueDetailsTableViewController {
			controller.issue = cell.issue
		}

	}

}

