//
//  RepositoryCollectionViewCell.swift
//  test-ios
//
//  Created by carlos chaguendo on 8/05/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

class RepositoryCollectionViewCell: UICollectionViewCell {

	override func layoutSubviews() {
		super.layoutSubviews()
		backgroundColor = Colors.navbar_back
	}

}
