//
//  IssueDetailsViewController.swift
//  test-ios
//
//  Created by carlos chaguendo on 20/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import LTMorphingLabel
import IoniconsSwift

class IssueDetailsTableViewController: UITableViewController {


	@IBOutlet weak var headerView: UIView!
//
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var avatarImage: UIImageView!
	@IBOutlet weak var reporterLabel: UILabel!
	@IBOutlet weak var reporterDetailLabel: LTMorphingLabel!
	@IBOutlet weak var issueDescriptionLabel: UILabel!
	@IBOutlet weak var responsibleAvatar: UIImageView!
	@IBOutlet weak var assigneeLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var priorityLabel: UILabel!

	var issue: Issue?
	var comments: [IssueComment] = []

	override func viewDidLoad() {
		super.viewDidLoad()


		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 250

		tableView.sectionHeaderHeight = UITableViewAutomaticDimension
		tableView.estimatedSectionHeaderHeight = 30;

		navigationController?.setNavigationBarHidden(false, animated: true)
		navigationController?.hidesBarsOnSwipe = false



		assigneeLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showUserSelector(_:))))


		guard let issue = self.issue else {
			preconditionFailure()
		}

		navigationItem.title = "#\(issue.id)"

		titleLabel.text = issue.title
		avatarImage.setImage(fromURL: issue.reporter?.avatar)



		reporterLabel.text = issue.reporter?.displayName
		reporterDetailLabel.text = "Created at \(issue.createdOn?.relativeTime ?? "")"
		issueDescriptionLabel.text = issue.raw

		updateTableViewHeaderViewHeight()

		responsibleAvatar.setImage(fromURL: issue.assignee?.avatar)
		assigneeLabel.text = issue.assignee?.displayName
		statusLabel.text = " \(issue.state?.rawValue ?? "JUM") "


		if let state = issue.state {
			statusLabel.backgroundColor = state.color
			statusLabel.textColor = state.textColor
		}


		let priority = NSMutableAttributedString(string: String.ionicon(of: .iosArrowUp), attributes: [
			NSForegroundColorAttributeName: IssueStatus.invalid.color,
			NSFontAttributeName: UIFont.ionicon(ofSize: 22)
		])

		priority.append(NSAttributedString(string: " \(issue.priority!)"))
		priorityLabel.attributedText = priority

		IssuesService.comments(of: "mayorgafirm", inRepository: "adivantus-iphone", forIssue: issue.id)
			.then { (result) -> Void in

				guard let values = result?.values else {
					return
				}

				values .forEach({ self.comments.append($0) })

				self.tableView.reloadData()

				print("comentarios \(result?.values)")
			}.always {

			}.catch(execute: presentError)

	}


	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return comments.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		let cell = UITableViewCell()
		if let comment = comments[safe: indexPath.row] {
			cell.textLabel?.text = comment.raw
			cell.textLabel?.numberOfLines = 0
		}

		return cell
	}

	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 40
	}

	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Comments"
	}

	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

		let v = view as! UITableViewHeaderFooterView
		v.backgroundView?.backgroundColor = UIColor.Hex(0xeeeeee)
		v.backgroundView?.layer.masksToBounds = false
		v.backgroundView?.layer.shadowColor = UIColor.clear.cgColor
		v.backgroundView?.layer.shadowOpacity = 3.0
		v.backgroundView?.layer.shadowRadius = 3

		v.textLabel?.textColor = UIColor.Hex(0x205081)
		v.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
		v.backgroundView?.layer.shadowOffset = CGSize(width: 0, height: 0)

	}



	@IBAction func showUserSelector(_ sender: Any) {


		// get a reference to the view controller for the popover
		let selectviewController = Storyboard.Issues.viewControllerWithClass(RepositorySelectViewController.self)
		let popController = UINavigationController(rootViewController: selectviewController)





		// set the presentation style

		popController.modalPresentationStyle = UIModalPresentationStyle.popover

		// set up the popover presentation controller
		popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
		popController.popoverPresentationController?.delegate = selectviewController
		popController.popoverPresentationController?.sourceView = assigneeLabel
		popController.popoverPresentationController?.sourceRect = CGRect(x: assigneeLabel.bounds.midX, y: assigneeLabel.bounds.midY, width: 0, height: 0)



		popController.popoverPresentationController?.popoverBackgroundViewClass = Pop.self


		// present the popover
		self.present(popController, animated: true, completion: nil)
	}




}

class Pop: UIPopoverBackgroundView {

	var _arrowOffset: CGFloat = 0.0
	var _arrowDirection: UIPopoverArrowDirection = .any
	var _arrowHeight: CGFloat = 10.0
	var _arrowImageView: UIImageView

	override init(frame: CGRect) {
		_arrowImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: _arrowHeight * 2, height: _arrowHeight * 2))
		_arrowImageView.backgroundColor = Colors.navbar_back

		super.init(frame: frame)
		addSubview(_arrowImageView)

		layer.shadowColor = UIColor.gray.withAlphaComponent(0.8).cgColor

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		var arrowCenter: CGPoint = .zero

		let backgroundFrame = self.frame;

		if _arrowDirection == .up {

			arrowCenter = CGPoint(x: backgroundFrame.size.width * 0.5 + self.arrowOffset, y: _arrowHeight);

		}
		print(arrowCenter)
		_arrowImageView.center = arrowCenter

		_arrowImageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 4))
	}

	/* Represents the the length of the base of the arrow's triangle in points.
     */
	public override static func arrowBase() -> CGFloat {
		return 10;
	}


	/* Describes the distance between each edge of the background view and the corresponding edge of its content view (i.e. if it were strictly a rectangle).
     */
	public override static func contentViewInsets() -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}


	public override static func arrowHeight() -> CGFloat {
		return 10;
	}


	/* The arrow offset represents how far from the center of the view the center of the arrow should appear. For `UIPopoverArrowDirectionUp` and `UIPopoverArrowDirectionDown`, this is a left-to-right offset; negative is to the left. For `UIPopoverArrowDirectionLeft` and `UIPopoverArrowDirectionRight`, this is a top-to-bottom offset; negative to toward the top.
     
     This method is called inside an animation block managed by the `UIPopoverController`.
     */
	public override var arrowOffset: CGFloat {
		get { return _arrowOffset }
		set { _arrowOffset = newValue; self.setNeedsLayout() }
	}


	/* `arrowDirection` manages which direction the popover arrow is pointing. You may be required to change the direction of the arrow while the popover is still visible on-screen.
     */
	public override var arrowDirection: UIPopoverArrowDirection {
		get { return _arrowDirection }
		set { _arrowDirection = newValue ; self.setNeedsLayout() }
	}


	/* This method may be overridden to prevent the drawing of the content inset and drop shadow inside the popover. The default implementation of this method returns YES.
     */
	public override class var wantsDefaultContentAppearance: Bool { return true }



}
