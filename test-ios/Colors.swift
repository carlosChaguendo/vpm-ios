//
//  Colors.swift
//  test-ios
//
//  Created by carlos chaguendo on 28/04/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

public struct Colors {

	public static let primary: UIColor = UIColor.Hex(0x205081)

	public static let navbar_back: UIColor = UIColor.Hex(0xf4f5f7)
	public static let navbar_button: UIColor = primary
	public static let navbar_title: UIColor = UIColor.Hex(0x42526e)
	public static let separator_color = UIColor.Hex(0xe7e7e7)

	public static let status_bar: UIColor = navbar_back
	public static let controller_background: UIColor = UIColor.Hex(0xeaecf0)

	public static let activity_indicator: UIColor = primary



}
